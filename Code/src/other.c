#include "other.h"

//-------------------------- Other function ------------------------------------


char utoa(uint8_t c)
{
    return (c + '0'); 
}

void razdel(uint16_t chis, char* str_V)
{
  uint8_t copy_ch = 0;
  uint8_t num = 0;
  while(chis>0)
  {
    copy_ch = chis % 10;
    str_V[num] = utoa(copy_ch);
    chis /= 10;
    num++;
  }
  str_V[num]  = '\0';
  flip(str_V);
}

void flip(char* str)
{
  char flp[15];
  signed char chi=0;
  char num=0;
  while(str[chi] != '\0')
  {
    flp[chi] = str[chi];
    chi++;
  }
  for(chi--; chi >= 0; num++, chi--)
    str[num] = flp[chi];
}

void combo(uint8_t L, uint8_t H, uint16_t* LH)
{
  *LH |= L;
  *LH |= H << 8;
}