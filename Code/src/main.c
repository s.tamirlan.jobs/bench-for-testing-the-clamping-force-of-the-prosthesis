#include "main.h"

char data_U[20];
uint8_t i = 0x00;
uint8_t config = 0x00;
uint8_t round = 0x01;
uint8_t flag_tx = 0x00;                 // this character may boolean
uint8_t bit_tx = 0x00;                  // this character may boolean        

//---------------------------- Main --------------------------------------------
int main( void )
{
  
  init_Led();
  init_Suart();
  init_UART();
  init_ADC();
  init_TIM4(); 
  interrupt_UART();
  //TIM4->CR1 |= TIM4_CR1_CEN;
  ADC1->CR1 |= ADC1_CR1_ADON;
  enableInterrupts();
  
  
  while(1)
  {
    //Mail_send(**_chr);
  }
}

//------------------------ INTERRUPT_HANDLER -----------------------------------
INTERRUPT_HANDLER(UART2_IRQ, 21)
{
  while((UART2->SR & UART2_CR2_RIEN) != UART2_CR2_RIEN);
  UART2->SR &= ~UART2_CR2_RIEN;
  uint8_t chek = UART2->DR;
  data_U[i++] = chek;
  if (chek == '\0')
  {
    i = 0;
    do_it(data_U);
  }
  else
  {
    //
  }
  
}

INTERRUPT_HANDLER(TIM4_IRQ, 23)
{
  TIM4->SR1 &= ~TIM4_SR1_UIF;
  flag_tx = 1;
  if(bit_tx == 1)
    Usart_tx_ON();
  else                                          // if(bit_tx == 0)
    Usart_tx_OFF();
}

//---------------------------- function ----------------------------------------
void do_it(char* input)
{
  uint8_t up = 0;
  switch(input[up++])
  {
  case VOLT:
     if(input[up] == '3')                                       // 3 V
      {   
        /* need to create characters
           uint8_t volt
           uint8_t mode or boolean mode
           volt = 0x03 
        */
        if(input[up+=2] == '1')                                 // 3V single mode
        {   /* mode = 0 (single) */    }
        else if(input[up] == '2')                               // 3V circle mode
        {   /* mode = 1 (circle) */  }
      }
     else if(input[up] == '9')                                  // 9 V
      {   
        /* volt = 0x09 */
        if(input[up+=2] == '1')                                 // 9V single mode
        {   /* mode = 0 (single) */   }
        else if(input[up] == '2')                               // 9V circle mode
        {   /* mode = 1 (circle) */   }
      }
     else if(input[up] == '1')                                  // 12 V
      {  
        /* volt = 0x0C */
        if(input[up+=2] == '1')                                 // 12V single mode
        {   /* mode = 0 (single) */   }
        else if(input[up] == '2')                               // 12V circle mode
        {   /* mode = 1 (circle) */   }       
      }
     else
      {/*ERROR*/}
    break;
  case START:
     if(input[up] == '1')
      {    
         /*
            need to create function Start()
            which included if-esle or switch or something
              - volt
              - mode
              - round
            * enable rele
            * second timer
            * ADCH & ADCL in massive
            * circle or single
         */
      }
     else
      {/*ERROR*/}
    break;
  case COM:
     if(input[up] == '1')                       // need to save on EEPROM
      {         }                               
     else if(input[up] == '2')
      {   
         send_str_SuartTX("Motorica");          // need to create massive of all data
                                                // and send to PC
                                                // may do soft uart_RX for find situation
      }
     else
      {/*ERROR*/}
    break;
  case CLEAR:
     if(input[up] == '1')                       // need to create function clear_massive()
      {         }                               
     else if(input[up] == '2')                  // need to create function clear_massive()
      {         }
     else
      {/*ERROR*/}
    break;
  case KALIBR:
      config = input[up];
    break;
  case CIRC:
      round = input[up];
    break;
  case QUIT:
    {
      /*code*/                                  // disable pin with ODR
    }
    break;
  default:
    {/*ERROR*/}
    break;
  }
}