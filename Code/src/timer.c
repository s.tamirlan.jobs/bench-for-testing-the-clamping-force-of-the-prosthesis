#include "timer.h"

void init_TIM4(void)
{
  CLK->PCKENR1 |= CLK_PCKENR1_TIM4;
  TIM4->PSCR = 0x02;                            // prescaller (2)
  TIM4->ARR = 0x32;                             // limit of counter(it is 240 DEC FORMAT)
  TIM4->CNTR = (uint8_t) 0;                    // counter value
  
  TIM4->CR1 |= TIM4_CR1_URS;                    // request source IRQ
  TIM4->CR1 &= ~TIM4_CR1_UDIS;                   // update disable
  TIM4->CR1 |= TIM4_CR1_ARPE;                   // auto-reload enable
  TIM4->IER |= TIM4_IER_UIE;                    // enable interrupt
}