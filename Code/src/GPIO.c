#include "GPIO.h"

void init_Rele(void)
{
  GPIOC->ODR &= ~GPIO_PIN_3;
  GPIOC->DDR |= GPIO_PIN_3;
  GPIOC->CR1 |= GPIO_PIN_3; 
  GPIOC->CR2 &= ~GPIO_PIN_3;
  
  GPIOC->ODR &= ~GPIO_PIN_4;
  GPIOC->DDR |= GPIO_PIN_4;
  GPIOC->CR1 |= GPIO_PIN_4; 
  GPIOC->CR2 &= ~GPIO_PIN_4;
}

void init_Led(void)
{
  GPIOE->ODR |= GPIO_PIN_5;
  GPIOE->DDR |= GPIO_PIN_5;
  GPIOE->CR1 |= GPIO_PIN_5; 
  GPIOE->CR2 &= ~GPIO_PIN_5;
}

void delay(unsigned long int time)
{
  while(time--);
}