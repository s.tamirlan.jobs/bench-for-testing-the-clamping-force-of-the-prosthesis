#include "UART.h"

void init_UART(void)
{
  // --------------------- RCC and Mode UART -----------------------------------
  // Alternative function
  // UART2_RX (PD6) - floating or pull-up               IN
  // UART2_TX (PD5) - push-pull or pseudo-open drain    OUT
  
  // GPIO_Init(GPIOD,GPIO_PIN_6,GPIO_MODE_IN_FL_NO_IT);                                 // RX
  GPIOD->DDR &= ~GPIO_PIN_6; // reset 0x00;
  GPIOD->CR1 &= ~GPIO_PIN_6; // reset 0x02;
  GPIOD->CR2 &= ~GPIO_PIN_6; // reset 0x00;
  // GPIO_Init(GPIOD,GPIO_PIN_5,GPIO_MODE_OUT_PP_HIGH_SLOW);                            // TX
  GPIOD->DDR |= GPIO_PIN_5;
  GPIOD->CR1 |= GPIO_PIN_5; 
  GPIOD->CR2 &= ~GPIO_PIN_5;
  // CLK_PeripheralClockConfig(CLK_PERIPHERAL_UART2, ENABLE);
  CLK->PCKENR1 |= CLK_PCKENR1_UART2;
  
  // ----------------------- Configure UART ------------------------------------
  //UART2_Init(9600, UART2_WORDLENGTH_8D, UART2_STOPBITS_1, UART2_PARITY_NO, UART2_SYNCMODE_CLOCK_DISABLE, UART2_MODE_TXRX_ENABLE);
  UART2->BRR1 = 0x0D;
  UART2->BRR2 = 0x00;
  UART2->CR1 &= ~UART2_CR1_M;           // length 8 bit
  UART2->CR3 &= ~UART2_CR3_STOP;        // 1 stop bit
  UART2->CR1 &= ~UART2_CR1_PCEN;        // parity bit control off 
  UART2->CR3 &= ~UART2_CR3_CKEN;        // clock for sinchron signal
  UART2->CR2 |= UART2_CR2_TEN;          // transmitter enable
  UART2->CR2 |= UART2_CR2_REN;          // receiver enable
  
  UART2->CR1 &= ~UART2_CR1_UARTD;       // just in case (off uart for energy saving)


}

void interrupt_UART(void)
{
// Software interrupt___________________________________________________________  
  // ----------------------- configure interrupt UART --------------------------
  /* UART2_ITConfig(UART2_IT_RXNE_OR, ENABLE); */
  // UART2->CR2 |= UART2_CR2_RIEN;
  // need to add?:       \
      Parity Error interrupt\
      Transmitter Interrupt Enable mask\
      Transmission Complete Interrupt Enable mask\
      IDLE Line Interrupt Enable mask
   
  // ---------------------- Set level interrupt --------------------------------

        
// Hardware interrupt___________________________________________________________        
  // ---------------------- Init interrupt ------------------------------------
  
  //UART2_ITConfig(UART2_IT_RXNE_OR, ENABLE);
  //UART2_ITConfig(UART2_IT_TC, ENABLE);
  UART2->CR2 |= UART2_CR2_RIEN;         // allow receiver interrupt
  //UART2->CR2 |= UART2_CR2_TIEN;         // if have not transferred
  //UART2->CR2 |= UART2_CR2_TCIEN;        // Complete transmitte data interrupt
  //EXTI_SetExtIntSensitivity(EXTI_PORT_GPIOD, EXTI_SENSITIVITY_RISE_ONLY);
  EXTI->CR1 |= EXTI_CR1_PDIS_1;         // enable interrupt rise
  enableInterrupts();
}

void Mail_send(char* stroka)
{
  uint16_t whole = 0x0000;
  char data_V[20];
  uint8_t Low, High;
  
  switch(*stroka)
  {
    case LOADING_chr:
      for(uint8_t i=0; i < 100; i++)
      {
        razdel(i, data_V);
        Send_string(LOADING);
        Send_string(data_V);
        Send_end();
      }
      break;
    case SPEED_chr:
      Send_string(SPEED);
      // taimer ustanovit or chetchik
      // taimer po vnutrenemu trigeru
      // perevod poluchivhivocya chisla v stroku razdel( )
      // otpravka znacheniya Send_string(...)
      Send_end();
      break;
    case CURRENT_chr:
      Send_string(CURRENT);
      // prosmotr DRL & DRH registrov on the another chanel ADC
      // vicheslenie
      // perevod poluchivhivocya chisla v stroku razdel( )
      // otpravka znacheniya Send_string(...)
      Send_end();
      break;
    case POWER_chr:
      Send_string(POWER);
      // vicheslenie power cherez ADC
      // perevod poluchivhivocya chisla v stroku razdel( )
      // otpravka znacheniya Send_string(...)
      Send_end();
      break;
    case DATA_chr:
      Low = ADC1->DRL;
      High = ADC1->DRH;
      combo(Low,High, &whole);
      razdel(whole, data_V);
      Send_string(DATA_ADC);
      Send_string(data_V);
      Send_end();
      break;
    case FACTOR_chr:
      Send_string(FACTOR);
      // vicheslenie
      // perevod poluchivhivocya chisla v stroku razdel( )
      // otpravka znacheniya Send_string(...)
      Send_end();
      break;
    default:
      break;
  }
}

void Send_string(char* str)
{
  while(*str)
    Send_char(*str++);
}

void Send_char(char chr)
{
  while((UART2->SR & UART2_CR2_TCIEN) != UART2_CR2_TCIEN);
  UART2->DR = chr;
  
}

void Send_end(void)
{
  Send_char(0xFF);
  Send_char(0xFF);
  Send_char(0xFF);
}