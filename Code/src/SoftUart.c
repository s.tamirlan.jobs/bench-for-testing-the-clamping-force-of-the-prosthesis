#include "SoftUart.h"

void init_Suart(void)
{
  Usart_tx_ON();
  GPIOC->DDR |= GPIO_PIN_5;
  GPIOC->CR1 |= GPIO_PIN_5;
  GPIOC->CR2 &= ~GPIO_PIN_5;
}

void send_SuartTX(uint8_t chr)
{
  uint8_t data_tx;
  data_tx = chr;
  
  bit_tx = 0x00;
  flag_tx = 0x00;
  
  Enable_Tim();
  while(flag_tx == 0);                          // start bit
  flag_tx = 0x00;
  
  for(int i=0; i<8; i++)                        
  {
    bit_tx = ((data_tx >> i) & ~(~0<<1));
    while(flag_tx == 0);                        // 8-bits
    flag_tx = 0;
  }
  
  bit_tx = 0x01;
  while(flag_tx == 0);                          // stop bit
  Disable_Tim();
  TIM4->CNTR = (uint8_t) 0;
}

void send_str_SuartTX(char* str)
{
  while(*str != '\0')
    send_SuartTX(*str++);
}