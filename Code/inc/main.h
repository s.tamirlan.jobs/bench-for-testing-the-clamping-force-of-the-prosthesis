#ifndef __main_H
#define __main_H

#ifdef USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *   where the assert_param error has occurred.
  * @param file: pointer to the source file name
  * @param line: assert_param error line source number
  * @retval : None
  */
void assert_failed(u8* file, u32 line)
{ 
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}
#endif

#define VOLT            'V'

#define VOLT_3          "V3" 
#define VOLT_9          "V9" 
#define VOLT_12         "V12" 

#define MODE            'R'
#define MODE_SINGLE     "R1" 
#define MODE_CIRCLE     "R2"

#define START           'S'
#define START_1         "S1"

#define COM             'D'
#define SAVE            "D1" 
#define SEND            "D2"

#define CLEAR           'C'
#define CLEAR_NOT_ALL   "C1"
#define CLEAR_ALL       "C2"

#define KALIBR          'M'
#define KALIBR_I        "M"     // 0xXX 0x00 0x00 0x00 

#define CIRC            'N'
#define CIR_K           "N"     // 0xYY 0x00 0x00 0x00

#define QUIT            'A'


#include "stm8s.h"
#include "ADC.h"
#include "UART.h"
#include "other.h"
#include "timer.h"
#include "GPIO.h"
#include "SoftUart.h"

void do_it(char* input);

#endif