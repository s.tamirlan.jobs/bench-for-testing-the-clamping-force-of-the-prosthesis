#ifndef __OTHER_H
#define __OTHER_H

#include "stm8s.h"


char utoa(uint8_t c);
void razdel(uint16_t chis, char* str_V);
void flip(char* str);
void combo(uint8_t L, uint8_t H, uint16_t* LH);


#endif /*   __OTHER_H    */