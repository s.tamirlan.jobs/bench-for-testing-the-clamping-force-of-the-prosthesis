#ifndef __UART_H
#define __UART_H

#include "stm8s.h"
#include "other.h"

#define LOADING_chr         'L'
#define LOADING         "page4.j0.val="                 // not vazno
#define SPEED_chr           'S'
#define SPEED           "page5.n0.val="                 // send once
#define CURRENT_chr         'C'
#define CURRENT         "page5.n1.val="                 // send continius
#define POWER_chr           'P'
#define POWER           "page5.n2.val="                 // send continius
#define DATA_chr            'D'
#define DATA_ADC        "page8.n1.val="                 // send continius
#define FACTOR_chr          'F'
#define FACTOR          "page8.n2.val="                 // send once

extern uint8_t watch;

void init_UART(void);
void interrupt_UART(void);

void Send_char(char chr);
void Send_string(char* str);
void Send_end(void);
void Mail_send(char* stroka);

#endif