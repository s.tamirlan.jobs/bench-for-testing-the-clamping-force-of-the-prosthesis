#ifndef __GPIO_H
#define __GPIO_H

#include "stm8s.h"

void delay(unsigned long int time);
void init_Led(void);
void init_Rele(void);

#endif  /*__GPIO_H*/