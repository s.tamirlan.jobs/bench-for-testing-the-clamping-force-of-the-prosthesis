#ifndef __SOFTUART_H
#define __SOFTUART_H

#include "stm8s.h"

#define Usart_tx_OFF()   (GPIOC->ODR &= ~GPIO_PIN_5)
#define Usart_tx_ON()    (GPIOC->ODR |= GPIO_PIN_5)     

#define Enable_Tim()    (TIM4->CR1 |= TIM4_CR1_CEN)
#define Disable_Tim()   (TIM4->CR1 &= ~TIM4_CR1_CEN)

extern uint8_t flag_tx;
extern uint8_t bit_tx;


void init_Suart(void);
void send_SuartTX(uint8_t chr);
void send_str_SuartTX(char* str);

#endif /* __SOFTUART_H */